/* FIRST LAST NAMES - CS 291 - PROGRAM # Fall 2016
   dog class - implementation dog.cpp
*/
#include "dog.h"

Dog::Dog() { //default constructor
	name ="testname";
	age=0;
	//breed="";
	//color="gray";
	//gender='M';
}

Dog::Dog(string name, int age) :Dog() { //Constructor Delegation. calls the default constructo above
	this->name=name;
	setAge(age);
}

void Dog::setAge(int age) {		//filters results
	if(age>=0)this->age=age;
}

void Dog::print() {
	printf("\tNamed: %s, Age: %d\n", name.c_str(), age);
}
