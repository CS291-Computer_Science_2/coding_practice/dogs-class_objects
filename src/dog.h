/* FIRST LAST NAMES - CS 291 - PROGRAM # Fall 2016
   base class for this project
*/

#ifndef DOG_H		//MACRO - makes *.h run only once to avoid errors
#define DOG_H		//
#include <string>
using namespace std;

class Dog {		//class is a reusable snippet (to store and query)
	public:
		Dog();		//default constructor (storage and query)
		Dog (string name, int age);		//allow access to private vars

		virtual void print(); //????  Print Properties
		void setAge(int age);
		int getAge() {
			return age;
		}
		string getName() {
			return name;
		}

		//string name;
		//string breed;
		//string color;
	private:
		int age;
		string name;
		//char gender;
};
#endif
