////////////////////////////////////////////////////////////////////////////////////////////
//  Name:
//	Date:
//	Project:
//	Pseudo Code: (1) Create structure for a dog(dog.h)
//				 (2) dog.cpp sets what dogs can do
//				 (3) main.cpp used to make a new dog and have them perform different actions
////////////////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <string>
#include "dog.h"
using namespace std;


int main(int argc, char** argv) {
	cout <<"Dogs are something, but some people like them!\n";
	Dog *dg = new Dog("Duke",21);
	cout<<"My dog is";
	dg->print();
	
	dg->setAge(9);	
	dg->print();
	cout<<"My dog is now: ";
	dg->print();
	
	delete dg;	

	system("pause");
	return 0;
}
